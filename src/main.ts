import "@babel/polyfill";
import "mutationobserver-shim";
import Vue from "vue";
import "./plugins/bootstrap-vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import { BootstrapVueIcons } from "bootstrap-vue";

Vue.config.productionTip = false;
Vue.prototype.$eventHub = new Vue();
Vue.use(BootstrapVueIcons);

// Start point of the App
new Vue({
  router,
  store,
  render: h => h(App),
}).$mount("#app");
