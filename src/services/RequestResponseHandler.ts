import axios, { AxiosRequestConfig } from "axios";
/**
 * Common class to forward HTTP Requests
 */
export default class RequestResponseHandler {
  private static config: AxiosRequestConfig = {
    headers: { authorization: "Bearer " + process.env.VUE_APP_ACCESS_TOKEN }
  };
  public static restGet = async (endpoint: string) => {
    const resp = await axios.get(endpoint, RequestResponseHandler.config);
    return resp;
  };
}
