import apis from "@/commons/apis";
import RequestResponseHandler from "./RequestResponseHandler";
/**
 * TS Class to interact with Rest API for search mechanism
 */
export default class SearchService {
  public static findWines = async (query: string = "merlot") => {
    const resp = await RequestResponseHandler.restGet(apis.search(query));
    return resp.data;
  };
}
