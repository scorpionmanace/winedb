/**
 * for I8N
 * Global strings and messages
 */
export default class Messages {
  public static title: string = "SAQ Wine Database";
  public static SQAWebsite: string = "SQA Website";
  public static languages: string = "Languages";
  public static english: string = "English";
  public static french: string = "French";
  public static amberAles: string = "Amber Ales";
  public static beerUnder10: string = "Beer under $10";
  public static merlot: string = "Merlot";
  public static learnMore: string = "Learn More...";
}
