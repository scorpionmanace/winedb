/**
 * Common place to store all the APIs
 */
export default class apis {
  private static endpoint: string = process.env.VUE_APP_ENDPOINT;
  static search = (query: string = ""): string =>
    `${apis.endpoint}/search/v2${query ? "?q=" + query : ""}`;
}
