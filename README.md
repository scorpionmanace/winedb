# winedb

```
Vue based application to demontrate search based on Canadian SAQ Website Results, using Coveo API.
```

## Steps

```
- Clone the application
- Run: yarn install
- Run: yarn run serve:dev
- Enjoy application on localhost
```

## Project setup

```
yarn install
```

### Compiles and hot-reloads for development with environment variables

```
yarn run serve:dev
```

### Compiles and hot-reloads for development

```
yarn run serve
```

### Compiles and minifies for production

```
yarn run build
```

### Run your tests

```
yarn run test
```

### Lints and fixes files

```
yarn run lint
```

### Run your unit tests

```
yarn run test:unit
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
